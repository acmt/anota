# Anota Python

Annotation tool in Python, to be called standalone (for example, on a shell script) or inside any python script (for example, as a function). It uses GTK+ as the framework.